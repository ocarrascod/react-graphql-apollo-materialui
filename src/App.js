import React, { Component } from "react";
import ViewPersons from "./components/Persons/ViewPersons";
import { allPersons } from './queries/persons'
import { graphql } from "react-apollo";
import Container from '@material-ui/core/Container';

class App extends Component {
  render() {
    const { data } = this.props;
    const { loading, error, allPersons } = data;
    return (
      <Container maxWidth="lg">
          <ViewPersons loading={loading} error={error} allPersons={allPersons}/>
      </Container>
    );
  }
}
export default graphql(allPersons, {
  options: { fetchPolicy: 'cache-and-network' }
})(App);