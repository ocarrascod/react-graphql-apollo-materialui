import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { List, ListItem, ListItemText } from '@material-ui/core';

const styles = theme => ({
  card: {
    width: '240px'
  },

  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class CardPerson extends React.Component{
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { expanded } = this.state ;
    const { classes, person, favorite, handleFavoriteClick } = this.props;
    const { films, homeworld, name, species, starships } = person;
    const homeworldName = homeworld !== null ? homeworld.name : "Unknown";
    const isFavorite = favorite ? {color: 'red'} : {color: null}
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton aria-label="Add to favorites"
            onClick={handleFavoriteClick}
            style={isFavorite}
          >
            <FavoriteIcon  />
          </IconButton>
          <IconButton
              className={classnames(classes.expand, {
                [classes.expandOpen]: expanded,
              })}
              onClick={this.handleExpandClick}
              aria-expanded={expanded}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
          <List>
            <Typography color="textSecondary" gutterBottom>
              Homeworld:
              </Typography>
              <ListItem key={homeworldName}>
                <ListItemText primary={homeworldName}/>
              </ListItem>
            </List>
            <List>
              <Typography color="textSecondary" gutterBottom>
                Films:
              </Typography>
              {films.map( film =>
                <ListItem key={film.id}>
                  <ListItemText primary={film.title}/>
                </ListItem>
            )}
            </List>
            <List>
              <Typography color="textSecondary" gutterBottom>
                Species:
              </Typography>
              {species.map( specie =>
                <ListItem key={specie.id}>
                  <ListItemText primary={specie.name}/>
                </ListItem>
            )}
            </List>
            <List>
              <Typography color="textSecondary" gutterBottom>
                Starships:
              </Typography>
              {starships.map( starship =>
                <ListItem key={starship.id}>
                  <ListItemText primary={starship.name}/>
                </ListItem>
            )}
            </List>
          </CardContent>
        </Collapse>
      </Card>
    )
  }
}

export default withStyles(styles)(CardPerson)