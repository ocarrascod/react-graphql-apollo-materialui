import React, { Component, Fragment } from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import CardPerson from "./CardPerson";
import { TextField, Grid, Typography, Divider } from "@material-ui/core";
import { withStyles } from '@material-ui/styles';

const styles = () => ({
  rootDiv: {
    display: "flex",
    flexFlow: "row wrap",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "space-evenly",
  },
  text: {
    width: "100%",
    justifyContent: "center",
    paddingBottom: '20px'
  }
});

class ViewPersons extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      favorites: {}
    }
  }

  handleFilterTextChange = e => {
    this.setState({ name: e.target.value });
  };

  handleFavoriteClick = (personID) => {
    const { favorites } = this.state
    const favoritesClone = { ...favorites }
    if (personID in favorites) {
      delete favoritesClone[personID]
    } else {
      favoritesClone[personID] = personID
    }
    this.setState({ favorites: favoritesClone });
  };


  componentDidUpdate(prevState) {
    if (this.state.name != prevState.name) {

    }
  }

  render() {

    const { loading, error, allPersons, classes } = this.props;
    const { favorites, name } = this.state;
    const allPersonsToShow = [];

    if (loading == false) {
      allPersons.forEach((person) => {
        let full_nameLower = person.name.toLowerCase()
        if (!full_nameLower) return;
        if (full_nameLower.indexOf(name.toLowerCase()) === -1) {
          return;
        }
        allPersonsToShow.push(
          <Grid item key={person.id}>
            <CardPerson
              key={person.id}
              person={person}
              favorite={person.id in favorites}
              handleFavoriteClick={() => this.handleFavoriteClick(person.id)}
            />
          </Grid>
        )
      })
    }
    return (
      <div className={classes.rootDiv}>
        <Typography xs={12} gutterBottom variant="h3">Star Wars Characters from a GraphQL API</Typography>
        <TextField
          id="person-name"
          label="Search by name"
          value={this.state.name}
          onChange={(e) => {
            this.handleFilterTextChange(e)
          }}
          className={classes.text}
        />
        <Divider variant="middle"/>
        <Fragment>
          {loading &&
            <div>
              <div><Typography gutterBottom variant="h5">Loading...</Typography></div>
              <div><CircularProgress size={100} thickness={6} /></div>
            </div>
          }
          {error &&
            <Typography gutterBottom variant="h5">Uupss! Something went wrong</Typography>
          }
          {(allPersons && loading === false) &&
            <Grid container justify="center" alignItems="flex-start" spacing={3}>
              {allPersonsToShow}
            </Grid>
          }
        </Fragment>
      </div>
    )
  }
}

export default withStyles(styles)(ViewPersons)