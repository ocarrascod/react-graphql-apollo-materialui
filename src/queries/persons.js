import { gql } from "apollo-boost";

export const allPersons = gql`
    query allPersons($name:String) {
      allPersons(filter: { name_contains: $name }){
        id,
        name,
        homeworld{
          name
        },
        species{
          id,
          name
        },
        starships{
          id,
          name
        },
        films{
          id,
          title
        }
      }
    }
  `;