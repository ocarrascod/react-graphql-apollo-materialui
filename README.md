# React-GraphQL-Apollo-MaterialUI

A Funny challange to learn graphql, apollo and how to make a boilerplate for react without using create-react-app.

[![LIVE APP](https://upload.wikimedia.org/wikipedia/commons/6/6c/Star_Wars_Logo.svg)](https://infallible-golick-f24342.netlify.com/)

## How to deploy
 
Just run:

```bash
yarn install
yarn build
```

note: the builded app have the CORS problem. Is a better idea run:

```bash
yarn start
```
for local testing
